import { Component, Input, OnInit } from '@angular/core';
import { HomePage } from 'src/app/pages/home/home.page';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-sidemenu',
  templateUrl: './sidemenu.component.html',
  styleUrls: ['./sidemenu.component.scss'],
})
export class SidemenuComponent implements OnInit {

  moto:string

  @Input() home:HomePage

 
  constructor(private data:DataService) {}

  ngOnInit() {
   
  }

  onclick(e){
    this.moto=e
    
  }
}
