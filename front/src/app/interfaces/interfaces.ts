
export interface Moto{
    id?: number,
    marca: string,
    modelo: string,
    year: string,
    foto: string,
    precio: string
}