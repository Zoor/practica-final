import { Injectable } from '@angular/core';
import { Moto } from '../interfaces/interfaces';


@Injectable({
  providedIn: 'root'
})

export class DataService {
  //  BASE_URL='http://motos.puigverd.org'
   BASE_URL="http://hector-llorca-7e3.alwaysdata.net/miapi"

   currentMoto:Moto


  constructor() { }

  
  setCurrentMoto(moto:Moto){
    this.currentMoto= moto
  }
  getCurrentMoto(){
    return this.currentMoto
  }

  async getMotos(){
    const info=await (await fetch(`${this.BASE_URL}/motos`)).json()
    return info
  }

  async getMotosBy(moto:string){
    const info=await (await fetch(`${this.BASE_URL}/motos?marca=${moto}`)).json()
    return info
  }

  async deleteMoto(id:number){
    const info=await fetch(`${this.BASE_URL}/moto/${id}`,{
      method:"DELETE",
    })
    return info
  }

  async postMoto(formData){
    //quitar "/foto" de la url para mi api

    const info=await fetch(`${this.BASE_URL}/moto`,{
      method: "POST",
      body:formData
    })
   console.log(await info.json())

  }

}