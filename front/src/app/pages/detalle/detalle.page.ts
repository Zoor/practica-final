import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Moto } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-detalle',
  templateUrl: './detalle.page.html',
  styleUrls: ['./detalle.page.scss'],
})
export class DetallePage implements OnInit {

  moto:Moto

  constructor(private data:DataService,private router:Router) { }

  ngOnInit() {
    this.moto=this.data.getCurrentMoto()
    
    if(!this.moto){
      this.router.navigate(['home'])
    }
  }

  delete(){
    this.data.deleteMoto(this.moto.id)
    this.router.navigate(['home'])
  }

}
