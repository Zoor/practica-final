import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Moto } from 'src/app/interfaces/interfaces';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  image: any =""|| "../../../assets/camara.svg"



  year: ""
  modelo: ""
  precio: ""
  marca: ""
  foto:''


  constructor(private data: DataService, private router: Router) { }

  ngOnInit() {
  }
  i(e) {
    this.foto=e[0]
    const reader = new FileReader();
    reader.readAsDataURL(e[0]);
    reader.onload = (_event) => {
      this.image = reader.result;
    }

  }

  add() {
    const formData = new FormData()

    formData.append('marca',this.marca)
    formData.append('modelo',this.modelo)
    formData.append('year',this.year)
    formData.append('foto',this.foto)
    formData.append('precio',this.precio)

    this.data.postMoto(formData)

  }

}
