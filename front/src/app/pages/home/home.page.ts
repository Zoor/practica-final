import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

  motos:any
  filtrar:string
  pages=[
    {
      title:"Ducati",
      foto:"https://1000marcas.net/wp-content/uploads/2019/12/Ducati-logo.png"
    },
    {
      title:"Yamaha",
      foto:"https://e7.pngegg.com/pngimages/875/92/png-clipart-yamaha-motor-company-yamaha-corporation-motorcycle-logo-motorcycle-company-logo.png"
    },
    {
      title:"Honda",
      foto:"https://upload.wikimedia.org/wikipedia/commons/7/7b/Honda_Logo.svg"
    },
    {
      title:"Todas"
    }
  ]

  constructor(private router:Router,private data:DataService) { 
    
  }
  ionViewDidEnter(){
    this.ngOnInit()
  }
  filtrarMotos(e){

    (async () => {
      if(e!="Todas") this.motos= await this.data.getMotosBy(e)
      else this.motos= await this.data.getMotos()
    })()
  }

  ngOnInit() {
    (async ()=>{
      this.motos=await this.data.getMotos()
    })()
  }

  next(){
    this.router.navigate(['add'])
  }
  
  detalle(e){
    this.data.setCurrentMoto(e)
    this.router.navigate(['detalle'])

  }

}
