const motosRouter = require('express').Router()
const config = require('../utils/config')
const { Pool } = require('pg')
let multer  = require('multer')

var storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, 'public/fotos');
     },
    filename: function (req, file, cb) {
        cb(null , file.originalname);
    }
});
const upload = multer({ storage: storage })
const pool = new Pool(config.ddbbConfig);


motosRouter.get('/motos',(request,response)=>{
    const marca=request.query.marca || ''
    console.log(marca)

    let consulta=''
    if(marca){
        consulta = `SELECT * FROM  motos where marca = '${marca}'`
    }else{
        consulta = "SELECT * FROM  motos"
    }

    pool.query(consulta, (error, results) => {
        if (error) {
            throw error
        }
        console.log(results)
        //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
        response.status(200).json(results.rows)
        
    });

})


motosRouter.get('/test',(request,response)=>{
    response.status(200).json("HOLA")
})

motosRouter.post('/moto', upload.single('foto'),(request,response)=>{
    const body = request.body
    
    const newMoto={
        ...body,
        foto:`http://hector-llorca-7e3.alwaysdata.net/miapi/fotos/${request.file.filename}`
    }

    const consulta= "Insert into motos values(default,$1,$2,$3,$4,$5)"
    
    pool.query(consulta,[newMoto.marca,newMoto.modelo,newMoto.foto,newMoto.year,newMoto.precio], (error, results) => {

        if (error) {
            throw error
        }
        //a qui retornem la el status 200 (OK) i en el cos de la resposta les motos en json
        response.send('Hola aixo es el que m\'arriva' + JSON.stringify(request.body))
    });
})
motosRouter.delete('/moto/:id',(request,response)=>{

    const consulta=`Delete from motos where id=${request.params.id}`
    
    pool.query(consulta,(error,res)=>{
        if(error){
            throw error
        }
        response.send('Borrat')

    })
})

module.exports = motosRouter