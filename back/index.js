const express = require("express"); // es el nostre servidor web
const cors = require('cors'); // ens habilita el cors recordes el bicing???
const config = require('./utils/config')
const logger = require('./utils/logger')
const motosRouter= require('./controllers/moto')
var path = require('path');
const app = express();
const baseUrl = '/miapi';
app.use(cors());
app.use('/img',express.static('public/fotos'));


app.use(express.urlencoded({ extended: false })); //per a poder rebre json en el request
app.use(express.json());
app.use(baseUrl,motosRouter);


//Inicialitzem el servei
const PORT = process.env.PORT || 3000; // Port
const IP = process.env.IP || null; // IP

app.listen(PORT, IP, () => {
   logger.info("El servidor está inicialitzat en el puerto " + PORT);
});
